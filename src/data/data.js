const cellData = {
  cell00: {
    value: 0,
    color: '',
  },
  cell01: {
    value: 0,
    color: '',
  },
  cell02: {
    value: 0,
    color: '',
  },
  cell03: {
    value: 0,
    color: '',
  },
  cell04: {
    value: 0,
    color: '',
  },
  cell10: {
    value: 0,
    color: '',
  },
  cell11: {
    value: 0,
    color: '',
  },
  cell12: {
    value: 0,
    color: '',
  },
  cell13: {
    value: 0,
    color: '',
  },
  cell14: {
    value: 0,
    color: '',
  },
  cell20: {
    value: 0,
    color: '',
  },
  cell21: {
    value: 0,
    color: '',
  },
  cell22: {
    value: 0,
    color: '',
  },
  cell23: {
    value: 0,
    color: '',
  },
  cell24: {
    value: 0,
    color: '',
  },
  cell30: {
    value: 0,
    color: '',
  },
  cell31: {
    value: 0,
    color: '',
  },
  cell32: {
    value: 0,
    color: '',
  },
  cell33: {
    value: 0,
    color: '',
  },
  cell34: {
    value: 0,
    color: '',
  },
  cell40: {
    value: 0,
    color: '',
  },
  cell41: {
    value: 0,
    color: '',
  },
  cell42: {
    value: 0,
    color: '',
  },
  cell43: {
    value: 0,
    color: '',
  },
  cell44: {
    value: 0,
    color: '',
  },
};

const bingoList = [
  { name: 'Capturer 3 Rattata', level: 1 },
  { name: 'Capturer 3 Roucoul', level: 1 },
  { name: 'Capturer 3 Nosferati', level: 1 },
  { name: 'Capturer Paras', level: 1 },
  { name: 'Obtenir la Carte', level: 1 },
  { name: 'Obtenir Pot Poudre', level: 1 },
  { name: 'Capturer Piafabec', level: 1 },
  { name: 'Capturer Férosinge', level: 1 },
  { name: 'Obtenir Reptincel', level: 1 },
  { name: 'Obtenir Herbizarre', level: 1 },
  { name: 'Obtenir Carabaffe', level: 1 },
  { name: 'Voir la Navette spatiale', level: 1 },
  { name: 'Démarrer avec Carapuce', level: 1 },
  { name: 'Démarrer avec Salameche', level: 1 },
  { name: 'Démarrer avec Bulbizarre', level: 1 },
  { name: 'Capturer Nidoran (mâle ou femelle)', level: 1 },
  { name: 'Capturer Mystherbe ou Chétiflor', level: 1 },
  { name: 'CT 12 ou Pistolet à O', level: 1 },
  { name: 'CT 45 ou Attraction', level: 1 },
  { name: 'Obtenir Corde Sortie', level: 1 },
  { name: 'Fossile Dôme ou Nautile', level: 1 },
  { name: 'Battre 3 Montagnards', level: 2 },
  { name: 'Capturer Mélofée', level: 2 },
  { name: 'Obtenir Roucoups', level: 2 },
  { name: 'CT 09 ou Balle-Graine', level: 2 },
  { name: 'Obtenir Parasect', level: 2 },
  { name: 'Capturer Mystherbe ou Chétiflor', level: 2 },
  { name: 'Obtenir 2 baies', level: 2 },
  { name: '10 Pokemons Obtenue', level: 2 },
  { name: 'Utiliser aucune CTs', level: 2 },
  { name: 'Capturer Rondoudou', level: 2 },
  { name: 'Obtenir TV ABC', level: 2 },
  { name: 'Battre ou Capturer 1 Pikachu', level: 2 },
  { name: 'Obtenir/Capturer 1 Magicarp', level: 2 },
  { name: 'Obtenir 1 Pépite', level: 2 },
  { name: 'Obtenir 2 Pierre Lune', level: 2 },
  { name: 'CT 05 ou Hurlement', level: 2 },
  { name: 'Obtenir Canarticho', level: 3 },
  { name: 'Capturer Nidorino ou Nidorina', level: 3 },
  { name: 'Capturer Dardagnan or Papilusion', level: 3 },
  { name: 'Obtenir la Canne', level: 3 },
  { name: 'Capturer Miaouss', level: 3 },
  { name: 'Obtenir Mr. Mime', level: 3 },
  { name: 'Obtenir Nosferalto', level: 3 },
  { name: 'Obtenir Mélodelfe', level: 3 },
  { name: 'Obtenir le Mémorydex', level: 3 },
  { name: 'CT28 Tunnel', level: 3 },
  { name: 'Obtenir le Passe Bateau', level: 3 },
  { name: 'Obtenir le Bon de commande', level: 3 },
  { name: 'Capturer Taupiqueur', level: 3 },
  { name: 'Battre le Rival à Jadielle', level: 3 },
  { name: '0 Pokédollar', level: 3 },
  { name: '75,000 Pokédollar', level: 3 },
  { name: 'Utiliser aucun rappels', level: 3 },
  { name: 'Obtenir 2 super bonbon', level: 3 },
  { name: 'Avoir 3 pokemons niveau 30 ou +', level: 3 },
  { name: 'Obtenir 8 CTs', level: 3 },
  { name: 'Obtenir 2 Badges', level: 3 },
  { name: 'Capturer Arbok or Sablaireau', level: 3 },
  { name: '30 Pokemon vus', level: 3 },
  { name: 'Battre les 12 dresseurs dans le Mt Sélénite', level: 3 },
  { name: 'Battre 5 scouts', level: 3 },
  { name: 'Relachez le starter avant le level 11.', level: 4 },
  { name: '20 Pokemon obtenue', level: 4 },
  { name: 'Obtenir Triopikeur', level: 4 },
  { name: 'Obtenir Florizarre', level: 4 },
  { name: 'Obtenir Dracaufeu', level: 4 },
  { name: 'Obtenir Leviator', level: 4 },
  { name: 'Rentrer dans aucun Centre Pokemon', level: 4 },
  { name: 'Battre les 8 dresseurs sur la Route 3', level: 4 },
  { name: 'Obtenir CS 5 (Flash)', level: 4 },
  { name: 'Obtenir 3 Pokemons différents de type poison', level: 4 },
  { name: 'Avoir 1 Pokemon au level 40 ou +', level: 4 },
  { name: '30 Pokemon obtenus', level: 4 },
];

const calculTables = [
  [4, 3, 2, 1, 1],
  [4, 3, 3, 2, 1],
  [3, 3, 2, 2, 1],
  [3, 3, 2, 1, 1],
];

const getBingoElement = (list, level) => {
  const listFiltered = list.filter((l) => l.level === level);
  return listFiltered[Math.floor(Math.random() * listFiltered.length)];
};

const getBingo = () => {
  let bingoClone = [...bingoList];
  const table = [];
  for (let col = 0; col < 5; col++) {
    table[col] = [];
    const calculTable = calculTables[Math.floor(Math.random() * calculTables.length)];
    let calculTableIndex = 0;
    for (let row = 0; row < 5; row++) {
      const element = getBingoElement(bingoClone, calculTable[calculTableIndex]);
      table[col][row] = element;
      bingoClone = bingoClone.filter((b) => b.name !== element.name);
      calculTableIndex++;
    }
  }
  return table;
};

export { getBingo, cellData };
