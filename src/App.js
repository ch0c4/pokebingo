import './App.css';
import { cellData, getBingo } from './data/data';
import { useState } from 'react';
import Timer from './components/Timer';

const App = () => {
  const [bingoList, setBingoList] = useState(getBingo());
  const cellDataClone = { ...cellData };
  const [cell, setCell] = useState(cellDataClone);

  const roll = () => {
    const cellDataClone = { ...cellData };
    setCell(cellDataClone);
    setBingoList(getBingo());
  };

  const getColor = (value) => {
    if (value === 1) return 'table-success';
    if (value === 2) return 'table-danger';
    return '';
  };

  const handleCellClick = (cellName) => {
    const newCell = { ...cell };
    let newValue = newCell[cellName].value + 1;
    if (newValue > 2) newValue = 0;
    newCell[cellName] = {
      value: newValue,
      color: getColor(newValue),
    };
    setCell(newCell);
    console.log(cell);
  };

  return (
    <div className="App mt-4">
      <div className="d-flex flex-column justify-content-around">
        <div className="d-flex flex-row justify-content-around">
          <h2>Pokébingo</h2>
          <button type="button" className="btn btn-secondary" onClick={roll}>
            Roll
          </button>
        </div>
        <div className="d-flex flex-row justify-content-around">
          <div className="d-flex flex-column justify-content-around ml-4">
            <div>
              <h2 className="mb-3">Chrono</h2>
              <Timer />
            </div>
            <div>
              <h2>Astuce</h2>
              <ul className="list-group">
                <li>1 clic sur une cellule défini l'objectif en cours</li>
                <li>2 clic sur une cellule défini l'objectif terminé</li>
              </ul>
            </div>
          </div>
          <table style={{ maxWidth: '80vw' }} className="table table-bordered mt-4">
            <thead>
              <tr>
                <th>Diag HG/BD</th>
                <th scope="col">Col 1</th>
                <th scope="col">Col 2</th>
                <th scope="col">Col 3</th>
                <th scope="col">Col 4</th>
                <th scope="col">Col 5</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">Row 1</th>
                {bingoList[0]?.map((row, index) => (
                  <td
                    key={index}
                    className={cell[`cell0${index}`].color}
                    style={{ cursor: 'pointer' }}
                    onClick={() => handleCellClick(`cell0${index}`)}
                  >
                    {row.name}
                  </td>
                ))}
              </tr>
              <tr>
                <th scope="row">Row 2</th>
                {bingoList[1]?.map((row, index) => (
                  <td
                    key={index}
                    className={cell[`cell1${index}`].color}
                    style={{ cursor: 'pointer' }}
                    onClick={() => handleCellClick(`cell1${index}`)}
                  >
                    {row.name}
                  </td>
                ))}
              </tr>
              <tr>
                <th scope="row">Row 3</th>
                {bingoList[2]?.map((row, index) => (
                  <td
                    key={index}
                    className={cell[`cell2${index}`].color}
                    style={{ cursor: 'pointer' }}
                    onClick={() => handleCellClick(`cell2${index}`)}
                  >
                    {row.name}
                  </td>
                ))}
              </tr>
              <tr>
                <th scope="row">Row 4</th>
                {bingoList[3]?.map((row, index) => (
                  <td
                    key={index}
                    className={cell[`cell3${index}`].color}
                    style={{ cursor: 'pointer' }}
                    onClick={() => handleCellClick(`cell3${index}`)}
                  >
                    {row.name}
                  </td>
                ))}
              </tr>
              <tr>
                <th scope="row">Row 5</th>
                {bingoList[4]?.map((row, index) => (
                  <td
                    key={index}
                    className={cell[`cell4${index}`].color}
                    style={{ cursor: 'pointer' }}
                    onClick={() => handleCellClick(`cell4${index}`)}
                  >
                    {row.name}
                  </td>
                ))}
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th scope="row">Diag BG/HD</th>
                <td>Col 1</td>
                <td>Col 2</td>
                <td>Col 3</td>
                <td>Col 4</td>
                <td>Col 5</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  );
};

export default App;
