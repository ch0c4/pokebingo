import React, { useRef, useState } from 'react';

const Timer = () => {
  const Ref = useRef(null);
  const [timer, setTimer] = useState('00:00:00');
  const [textTimer, setTextTimer] = useState('Start');

  const getTimeRemaining = (e) => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / 1000 / 60 / 60) % 60);
    return { total, hours, minutes, seconds };
  };

  const startTimer = (e) => {
    let { total, hours, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      setTimer(
        (hours > 9 ? hours : '0' + hours) +
          ':' +
          (minutes > 9 ? minutes : '0' + minutes) +
          ':' +
          (seconds > 9 ? seconds : '0' + seconds)
      );
    }
  };

  const clearTimer = (e) => {
    setTimer('02:00:00');

    if (Ref.current) clearInterval(Ref.current);
    Ref.current = setInterval(() => {
      startTimer(e);
    }, 1000);
  };

  const getDeadTime = () => {
    const deadLine = new Date();
    deadLine.setSeconds(deadLine.getSeconds() + 7200);
    return deadLine;
  };

  const handleClickReset = () => {
    clearTimer(getDeadTime());
    setTextTimer('Reset');
  };

  return (
    <div className="Timer d-flex flex-lg-row flex-sm-column justify-content-around">
      <button className="btn btn-secondary mb-3" onClick={handleClickReset}>
        {textTimer}
      </button>
      <h3>{timer}</h3>
    </div>
  );
};

export default Timer;
